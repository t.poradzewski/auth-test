import React, { Dispatch } from 'react';
import { useDispatch } from 'react-redux';
import { updateCurrentPath } from '../../../store/actions/root.actions';

const Home: React.FC = () => {
  const dispatch: Dispatch<any> = useDispatch();
  dispatch(updateCurrentPath('home', ''));

  return (
    <>
      <h1 className="h3 mb-2 text-gray-800">Dashboard</h1>
    </>
  );
};

export default Home;
