import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import TopMenu from '../../components/TopMenu/TopMenu';
import LeftMenu from '../../components/LeftMenu/LeftMenu';
import Home from './Home/Home';
import Settings from './Settings/Settings';

const Admin: React.FC = () => (
  <>
    <LeftMenu />
    <div id="content-wrapper" className="d-flex flex-column">
      <div id="content">
        <TopMenu />
        <div className="container-fluid">
          <Switch>
            <Route path="/home">
              <Home />
            </Route>
            <Route path="/settings">
              <Settings />
            </Route>
            <Route path='/' render={() => (
              <Redirect to="/home" />
            )} />
          </Switch>
        </div>
      </div>
    </div>
  </>
);

export default Admin;
