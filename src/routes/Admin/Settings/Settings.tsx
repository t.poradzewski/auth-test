import React, { Dispatch, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Container,
  Table,
} from 'semantic-ui-react';
import { updateCurrentPath } from '../../../store/actions/root.actions';
import { getSettings } from '../../../store/actions/user.actions';
import { IState } from '../../../store/models/root.interface';

const Settings: React.FC = () => {
  const dispatch: Dispatch<any> = useDispatch();
  useEffect(() => {
    dispatch(updateCurrentPath('settings', ''));
    dispatch(getSettings());
  }, [dispatch]);
  const settings: Array<any> = useSelector((state: IState) => state.user.settings);
  return (
    <>
      <h1 className="h3 mb-2 text-gray-800">Settings</h1>
      <Container text>
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Rarity</Table.HeaderCell>
              <Table.HeaderCell>Super type</Table.HeaderCell>
              <Table.HeaderCell>Sub type</Table.HeaderCell>
              <Table.HeaderCell>Set</Table.HeaderCell>
              <Table.HeaderCell>Hp</Table.HeaderCell>
              <Table.HeaderCell>No.</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {settings ? settings.map(({
              id,
              name,
              rarity,
              supertype,
              subtype,
              set,
              hp,
              number,
            }) => (
              <Table.Row key={id}>
                <Table.Cell>{name}</Table.Cell>
                <Table.Cell>{rarity}</Table.Cell>
                <Table.Cell>{supertype}</Table.Cell>
                <Table.Cell>{subtype}</Table.Cell>
                <Table.Cell>{set}</Table.Cell>
                <Table.Cell>{hp}</Table.Cell>
                <Table.Cell>{number}</Table.Cell>
              </Table.Row>
            )) : (
              <Table.Row>
                <Table.Cell>Loading...</Table.Cell>
              </Table.Row>
            )}
          </Table.Body>
        </Table>
      </Container>
    </>
  );
};

export default Settings;
