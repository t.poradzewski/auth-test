import { Route, RouteProps } from 'react-router';
import { Redirect, withRouter } from 'react-router-dom';
import React from 'react';
import { useSelector, connect } from 'react-redux';
import { IState } from '../../store/models/root.interface';
import { IUserState } from '../../store/models/user.interface';

export function AdminRouteGuard({ children, ...rest }: RouteProps): JSX.Element {
  const user: IUserState = useSelector((state: IState) => state.user);

  return (
    <Route
      {...rest}
      render={() => (user.loggedIn ? (
        children
      ) : (
        <Redirect
          to="/login"
        />
      ))}
    />
  );
}
export default withRouter(connect(
  (state) => state,
)(AdminRouteGuard));
