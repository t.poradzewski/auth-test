import { Route, Redirect, RouteProps } from 'react-router';
import { withRouter } from 'react-router-dom';
import React from 'react';
import { useSelector, connect } from 'react-redux';
import { IState } from '../../store/models/root.interface';
import { IUserState } from '../../store/models/user.interface';
import Login from './Login/Login';


export function AuthRouteGuard({ children, ...rest }: RouteProps): JSX.Element {
  const user: IUserState = useSelector((state: IState) => state.user);
  return (
    <Route
      {...rest}
      render={() => (user.loggedIn ? (
        <Redirect
          to={{
            pathname: '/home',
          }}
        />
      ) : <Login />)}
    />
  );
}

export default withRouter(connect(
  (state) => state,
)(AuthRouteGuard));
