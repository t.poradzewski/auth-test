import { IUserState } from './user.interface';


export interface IRootPageState {
  area: string;
  subArea: string;
}

export interface IRootState {
  page: IRootPageState;
}
export interface IState {
  root: IRootState;
  user: IUserState;
}

export interface IActionBase {
  type: string;
  [prop: string]: any;
}
