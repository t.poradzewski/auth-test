export interface IUserState {
  loggedIn?: boolean;
  loading?: boolean;
  me?: IMeState;
  settings?: any;
}

export interface IMeState {
  id: number;
  name: string;
  firstName: string;
  lastName: string;
  token: string;
}
