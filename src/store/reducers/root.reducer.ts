import { combineReducers, Reducer } from 'redux';
import { UPDATE_CURRENT_PATH } from '../actions/root.actions';
import { IRootState, IActionBase, IState } from '../models/root.interface';
import accountReducer from './user.reducer';


const initialState: IRootState = {
  page: { area: 'home', subArea: '' },
};

function rootReducer(state: IRootState = initialState, action: IActionBase): IRootState {
  switch (action.type) {
    case UPDATE_CURRENT_PATH:
      return { ...state, page: { area: action.area, subArea: action.subArea } };
    default:
      return state;
  }
}

const rootReducers: Reducer<IState> = combineReducers({
  root: rootReducer,
  user: accountReducer,
});


export default rootReducers;
