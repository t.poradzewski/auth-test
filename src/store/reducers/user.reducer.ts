import { IActionBase } from '../models/root.interface';
import { IUserState } from '../models/user.interface';

import { userConstants } from '../constants';

const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user') as string) : false;
const initialState = user ? { loggedIn: true, user } : {};

function accountReducer(state: IUserState = initialState, action: IActionBase): IUserState {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loading: true,
        me: action.user,
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        me: action.user,
      };
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
    case userConstants.GETSETTINGS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case userConstants.GETSETTINGS_SUCCESS:
      return {
        ...state,
        loading: false,
        settings: action.settings,
      };
    case userConstants.GETSETTINGS_FAILURE:
      return {
      };
    default:
      return state;
  }
}
export default accountReducer;
