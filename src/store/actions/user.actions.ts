import { Dispatch } from 'react';
import { userConstants } from '../constants';
import { userService } from '../../services/user.service';
import { history } from '../../helpers/history';
import { IMeState } from '../models/user.interface';

export function login(name: string, password: string) {
  function request(user: string) { return { type: userConstants.LOGIN_REQUEST, user }; }
  function success(user: IMeState) { return { type: userConstants.LOGIN_SUCCESS, user }; }
  function failure(error: any) { return { type: userConstants.LOGIN_FAILURE, error }; }
  return (dispatch: Dispatch<any>) => {
    dispatch(request(name));

    userService.login(name, password)
      .then(
        (user) => {
          dispatch(success(user));
          history.push('/home');
        },
        (error) => {
          dispatch(failure(error));
        },
      );
  };
}

export function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}

export function getSettings() {
  function request() { return { type: userConstants.GETSETTINGS_REQUEST }; }
  function success(settings: any) { return { type: userConstants.GETSETTINGS_SUCCESS, settings }; }
  function failure(error: any) { return { type: userConstants.GETSETTINGS_FAILURE, error }; }
  return (dispatch: Dispatch<any>) => {
    dispatch(request());
    userService.getSettings()
      .then(
        (settings) => dispatch(success(settings)),
        (error) => dispatch(failure(error)),
      );
  };
}

export default {
  login,
  logout,
  getSettings,
};
