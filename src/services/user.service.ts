import { authHeader } from '../helpers';

const logout = () => {
  localStorage.removeItem('user');
};

const handleResponse = (response: any) => response.text().then((text: string) => {
  const data = text && JSON.parse(text);
  if (!response.ok) {
    if (response.status === 401) {
      logout();
      window.location.reload(true);
    }

    const error = (data && data.message) || response.statusText;
    return Promise.reject(error);
  }

  return data;
});

const login = (name: string, password: string) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ name, password }),
  };

  return fetch(`${process.env.REACT_APP_ENDPOINT}/users/authenticate`, requestOptions)
    .then(handleResponse)
    .then((user) => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('user', JSON.stringify(user));

      return user;
    });
};

const getSettings = async () => {
  const apiAddress = 'https://api.pokemontcg.io/v1/cards?';
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  const response = await fetch(`${apiAddress}name=${'pikachu'}`, requestOptions);
  console.log('response: ', response);
  const json = await response.json();
  console.log('json: ', json);
  if (response.status === 200) {
    return json.cards;
  }
  return Promise.reject(response);
};


export const userService = {
  login,
  logout,
  getSettings,
};

export default userService;
