import React, { useState, Dispatch } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { logout } from '../../store/actions/user.actions';
import { IState } from '../../store/models/root.interface';

function TopMenuAccount(): JSX.Element {
  const dispatch: Dispatch<any> = useDispatch();
  const name: string | undefined = useSelector((state: IState) => state.user.me?.name);
  const [isShow, setShow] = useState(false);

  return (

    <li className="nav-item dropdown no-arrow">
      <a
        className="nav-link dropdown-toggle"
        onClick={() => {
          setShow(!isShow);
        }}
        href="# "
        id="userDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <span className="mr-2 d-none d-lg-inline small cadet">{name}</span>
      </a>
      <div
        className={`dropdown-menu dropdown-menu-right shadow animated--grow-in ${(isShow) ? 'show' : ''}`}
        aria-labelledby="userDropdown"
      >
        <Link onClick={() => dispatch(logout())} className="dropdown-item" to="/login">
          Logout
        </Link>
      </div>
    </li>
  );
}

export default TopMenuAccount;
