import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { IState, IRootPageState } from '../../store/models/root.interface';

const LeftMenu: React.FC = () => {
  const [leftMenuVisibility, setLeftMenuVisibility] = useState(false);
  const page: IRootPageState = useSelector((state: IState) => state.root.page);

  function changeLeftMenuVisibility() {
    setLeftMenuVisibility(!leftMenuVisibility);
  }

  function getCollapseClass() {
    return (leftMenuVisibility) ? '' : 'collapsed';
  }

  return (
    <>
      <div className="toggle-area">
        <button className="btn btn-primary toggle-button" type="button" onClick={() => changeLeftMenuVisibility()}>
          <i className="fas fa-bolt" />
        </button>
      </div>
      <ul
        className={`navbar-nav bg-gradient-primary-green sidebar sidebar-dark accordion ${getCollapseClass()}`}
        id="collapseMenu"
      >
        <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
          <div className="sidebar-brand-icon icon-green rotate-n-15">
            <i className="fas fa-bolt" />
          </div>
          <div className="sidebar-brand-text mx-3">
            Dashboard
          </div>
        </a>
        <hr className="sidebar-divider my-0" />
        <li className={`nav-item ${page.area === 'home' ? 'active' : ''}`}>
          <Link className="nav-link" to="/home">
            <span>Dashboard</span>
          </Link>
        </li>
        <li className={`nav-item ${page.area === 'settings' ? 'active' : ''}`}>
          <Link className="nav-link" to="/settings">
            <span>Settings</span>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default LeftMenu;
