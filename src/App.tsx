import React from 'react';
import './styles/sb-admin-2.min.css';
import { Router, Switch } from 'react-router-dom';
import Login from './routes/Auth/Login/Login';
import Admin from './routes/Admin/Admin';
import { AdminRouteGuard } from './routes/Admin/AdminRouteGuard';
import { AuthRouteGuard } from './routes/Auth/AuthRouteGuard';
import './App.css';
import { history } from './helpers/history';

const App: React.FC = () => (
  <div className="App" id="wrapper">
    <Router history={history}>
      <Switch>
        <AuthRouteGuard path="/login">
          <Login />
        </AuthRouteGuard>
        <AdminRouteGuard>
          <Admin />
        </AdminRouteGuard>
      </Switch>
    </Router>
  </div>
);

export default App;
